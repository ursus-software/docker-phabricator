Phabricator image
=================

* Based on `ursus/nginx-php5-fpm`
* Uses supervisord
* Configuration
	+ mysql.host is set to the value of `$MYSQL_PORT_3306_TCP_ADDR`
	+ mysql.port is set to the value of `$MYSQL_PORT_3306_TCP_PORT`
	+ mysql.user is set to the value of `$MYSQL_USER` or `root`
	+ mysql.pass is set to the value of `$MYSQL_PASSWORD` or `$MYSQL_ENV_MYSQL_ROOT_PASSWORD`
	+ phabricator.base-uri is set to the value of `$PHABRICATOR_BASE_URI`
	+ phabricator.timezone is set to the value of `$PHABRICATOR_TIMEZONE`
	+ auth.require-approval is set to `true`
	+ repository.default-local-path is set to `/var/phabricator/repo`
	+ storage.local-disk.path is set to `/var/phabricator/uploads`
	+ pygments.enabled is set to `true`
#!/usr/bin/env bash
sleep 10
if [ -z "$MYSQL_PORT_3306_TCP_ADDR" -a ! -z "$MYSQL_ADDRESS" ]; then
	read MYSQL_PORT_3306_TCP_ADDR MYSQL_PORT_3306_TCP_PORT <<< "$(echo "$MYSQL_ADDRESS" | sed -e 's/:/ /g')"
fi
if [ -z "$MYSQL_PORT_3306_TCP_ADDR" ]; then
	echo "Phabricator depends on mysql/mariadb."
	echo "Link to a mysql or mariadb container or set the following environment variables:"
	echo "* MYSQL_PORT_3306_TCP_ADDR (required)"
	echo "* MYSQL_PORT_3306_TCP_PORT (required)"
	echo "	or insted of the above, set MYSQL_ADDRESS as 'host:port'"
	echo "* MYSQL_ROOT_PASSWORD or MYSQL_ENV_MYSQL_ROOT_PASSWORD"
	echo "* MYSQL_USER and MYSQL_PASSWORD or MYSQL_ENV_MYSQL_USER and MYSQL_ENV_MYSQL_PASSWORD"
	exit 1
fi
MYSQL_ROOT_PASSWORD="${MYSQL_ROOT_PASSWORD:-$MYSQL_ENV_MYSQL_ROOT_PASSWORD}"
MYSQL_USER="${MYSQL_USER:-${MYSQL_ENV_MYSQL_USER:-"root"}}"
MYSQL_PASSWORD="${MYSQL_PASSWORD:-${MYSQL_ENV_MYSQL_PASSWORD:-$MYSQL_ROOT_PASSWORD}}"
MYSQL_DATABASE="${MYSQL_DATABASE:-$MYSQL_ENV_MYSQL_DATABASE}"
PHABRICATOR_TIMEZONE="${PHABRICATOR_TIMEZONE:-"Europe/Berlin"}"
if [ -z "$MYSQL_ROOT_PASSWORD" -a -z "$MYSQL_USER" -a -z "$MYSQL_PASSWORD" ]; then
	echo "Either set MYSQL_ENV_MYSQL_ROOT_PASSWORD or MYSQL_ROOT_PASSWORD or MYSQL_USER and MYSQL_PASSWORD."
	exit 1
fi
if [ ! -z "$PHABRICATOR_DEBUG" -a "$PHABRICATOR_DEBUG" = "true" ]; then
	echo "Environment:
	* MYSQL_PORT_3306_TCP_ADDR = $MYSQL_PORT_3306_TCP_ADDR
	* MYSQL_PORT_3306_TCP_PORT = $MYSQL_PORT_3306_TCP_PORT
	* MYSQL_USER = $MYSQL_USER
	* MYSQL_PASSWORD = $MYSQL_PASSWORD
	* MYSQL_DATABASE = $MYSQL_DATABASE
	* PHABRICATOR_BASE_URI = $PHABRICATOR_BASE_URI
	* PHABRICATOR_TIMEZONE = $PHABRICATOR_TIMEZONE"
fi
echo "===> Setting up database connection..."
/opt/phabricator/bin/config set mysql.host "$MYSQL_PORT_3306_TCP_ADDR"
/opt/phabricator/bin/config set mysql.port "$MYSQL_PORT_3306_TCP_PORT"
/opt/phabricator/bin/config set mysql.user "$MYSQL_USER"
/opt/phabricator/bin/config set mysql.pass "$MYSQL_PASSWORD"
if [ ! -z "$MYSQL_DATABASE" ]; then
	/opt/phabricator/bin/config set storage.default-namespace	"$MYSQL_DATABASE"
fi
echo "===> Setting up phabricator..."
if [ ! -z "$PHABRICATOR_BASE_URI" ]; then
	/opt/phabricator/bin/config set phabricator.base-uri "'$PHABRICATOR_BASE_URI'"
fi
/opt/phabricator/bin/config set phabricator.timezone "$PHABRICATOR_TIMEZONE"
echo "===> Configuring phabricator..."
/opt/phabricator/bin/config set auth.require-approval true
/opt/phabricator/bin/config set repository.default-local-path "/var/phabricator/repo"
/opt/phabricator/bin/config set storage.local-disk.path "/var/phabricator/uploads"
/opt/phabricator/bin/config set pygments.enabled true
echo y | /opt/phabricator/bin/storage upgrade -f
echo "Running phd"
supervisorctl start phd